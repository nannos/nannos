
:orphan:

.. _sphx_glr_sg_execution_times:


Computation times
=================
**00:33.483** total execution time for 16 files **from all galleries**:

.. container::

  .. raw:: html

    <style scoped>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    </style>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready( function () {
        $('table.sg-datatable').DataTable({order: [[1, 'desc']]});
    } );
    </script>

  .. list-table::
   :header-rows: 1
   :class: table table-striped sg-datatable

   * - Example
     - Time
     - Mem (MB)
   * - :ref:`sphx_glr_examples_basic_plot_square_patch.py` (``../examples/basic/plot_square_patch.py``)
     - 00:33.483
     - 743.2
   * - :ref:`sphx_glr_examples_basic_plot_1d_grating.py` (``../examples/basic/plot_1d_grating.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_basic_plot_convergence.py` (``../examples/basic/plot_convergence.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_basic_plot_conversion_eff.py` (``../examples/basic/plot_conversion_eff.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_basic_plot_double_phc.py` (``../examples/basic/plot_double_phc.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_basic_plot_ellipse.py` (``../examples/basic/plot_ellipse.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_basic_plot_hole.py` (``../examples/basic/plot_hole.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_basic_plot_stress.py` (``../examples/basic/plot_stress.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_basic_plot_tangent.py` (``../examples/basic/plot_tangent.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_benchmarks_plot_benchmarks.py` (``../examples/benchmarks/plot_benchmarks.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_benchmarks_plot_kaggle.py` (``../examples/benchmarks/plot_kaggle.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_examples_benchmarks_plot_modal.py` (``../examples/benchmarks/plot_modal.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_tutorials_plot_gradient.py` (``../tutorials/plot_gradient.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_tutorials_plot_layer.py` (``../tutorials/plot_layer.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_tutorials_plot_permittivity_fft.py` (``../tutorials/plot_permittivity_fft.py``)
     - 00:00.000
     - 0.0
   * - :ref:`sphx_glr_tutorials_plot_shapely.py` (``../tutorials/plot_shapely.py``)
     - 00:00.000
     - 0.0
