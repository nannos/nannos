


   
=========
Citing us
=========

If you use Nannos in your research, we would appreciate you cite us 
:footcite:p:`nannos`.

.. footbibliography::

.. code-block:: bibtex

  @misc{nannos,
    author       = {Vial, Benjamin},
    title        = {nannos},
    year         = 2022,
    publisher    = {Zenodo},
    doi          = {10.5281/zenodo.11658672},
    url          = {https://doi.org/10.5281/zenodo.11658672},
  }

You may also cite this paper explaining the numerical method and giving examples 
:footcite:p:`vial2022`.

.. footbibliography::

.. code-block:: bibtex

  @article{vial2022,
    title = {Open-{{Source Computational Photonics}} with {{Auto Differentiable Topology Optimization}}},
    author = {Vial, Benjamin and Hao, Yang},
    year = {2022},
    month = jan,
    journal = {Mathematics},
    volume = {10},
    number = {20},
    pages = {3912},
    publisher = {Multidisciplinary Digital Publishing Institute},
    issn = {2227-7390},
    doi = {10.3390/math10203912},
    copyright = {http://creativecommons.org/licenses/by/3.0/},
    keywords = {computational photonics,topology optimization}
  }
